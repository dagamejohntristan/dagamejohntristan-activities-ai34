<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
    $mul_rows= [
    [ 'category' => 'Fantasy'],
    [ 'category' => 'Drama'],
    [ 'category' => 'Romance'],
    [ 'category' => 'Horror'],
    [ 'category' => 'Mystery']
];

    foreach ($mul_rows as $rows) {
    Category::create($rows);
        }
    }
}
