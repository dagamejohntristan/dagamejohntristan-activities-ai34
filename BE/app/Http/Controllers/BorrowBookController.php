<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\ReturnedBook;
use App\Models\BorrowedBook;
use Illuminate\Http\Request;
use App\Http\Requests\BorrowedBookRequest;

class BorowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borrowedBooks = BorowedBook::all();
        return response()->json([
            "message" => "BorrowedBook List",
            "data" => $borrowedBooks]);
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $borrowedBook = new BorrowedBook();

        $borrowedBook->copies = $request->copies;
        $borrowedBook->book_id = $request->book_id;
        $borrowedBook->patron_id = $request->patron_id;
        
        $book = Book::find($borrowedBooks->book_id);

        $minuscopiesfrombook = $books->copies - $borrowedBooks->copies;


        $borrowedBooks->save();
        $books->update(['copies' => $minuscopiesfrombook]);
        return responce()->json(["data"=> $borrowedBooks, $books]);
    
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $BorrowedBook = BorrowedBook::find($id);
        return response()->json($BorrowedBook);
    }

   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $borrowedBook = BorrowedBook::find($id);

        $returnedbook = new ReturnedBook();
        
        $returnedBook->copies = $request->copies;
        $returnedBook->Book_id = $request->Book_id;
        $returnedBook->patron_id = $request->patron_id;

        $books = Book::find($returnedBook->book_id);

        $addcopiestoBook = $books->copies + $returnedBook->copies;

        $returnedBook->save();
        $borrowedBook->delete();
        $books->update(['copies' => $addcopiestobook]);
        return response()->json(["message" => "Borrowed Books and updated books copies" , "datas " => $borrowedBooks, $books]);    
    }

    
    
}
