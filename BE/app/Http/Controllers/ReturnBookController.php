<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBorrowedBookRequest;
use Illuminate\Http\Request;

class ReturnController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $returnedBooks = ReturnedBook::all();
        return response()->json([
            "message" => "ReturnedBooks List",
            "data" => $returnedBooks]);
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $returnedBooks = ReturnedBook::find($id);
        return response()->json($returnedBooks);
    }
}
    