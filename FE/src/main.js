
import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import toastr from "toastr";
import VueFilterDateFormat from "@vuejs-community/vue-filter-date-format";
Vue.use(VueFilterDateFormat);
import router from "./routes/router";
import store from "./store/store";
import App from "../src/App.vue";
import Toast from "vue-toastification";

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.config.productionTip = false

import './assets/css/style.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "bootstrap-vue/dist/bootstrap-vue.min.css";

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueFilterDateFormat from "@vuejs-community/vue-filter-date-format";

Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 3,
  newestOnTop: true,
});


Vue.config.productionTip = false;
Vue.use(toastr);

new Vue({
  store,
  router,
  render: function (h) { return h(App)}
}).$mount('#app')

